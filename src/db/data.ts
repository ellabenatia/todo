import fs from 'fs/promises';
import {task} from '../interfaces/task.js'

export async function start(){
    try{
        await fs.readFile('./todo.json', 'utf8');

     } catch (err) {
      await fs.writeFile('./todo.json', JSON.stringify([],null,2));
     }
}

export async function getData(){
    const data_json = await fs.readFile('./todo.json', 'utf8');
    let data = JSON.parse(data_json) as task[];
    return data; 
}

export async function save(tasks:task[]){
    await fs.writeFile('./todo.json', JSON.stringify(tasks, null, 2));
}