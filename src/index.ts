import * as todo_func from "./myModule.js";
import {start} from "./db/data.js";
import * as db from './db/data.js';
import {task} from './interfaces/task.js'

 async function init(){    
    await start();
    let data:task[] = await db.getData();
    var myArgs = process.argv.slice(2);
        switch (myArgs[0]) {
            case 'create':
                myArgs.shift();
               data = await todo_func.create(data,myArgs);
               db.save(data);
                break;
            case 'read':
            console.table(await todo_func.read(await db.getData()));
                break;
            case 'update':
                data = await todo_func.update(data,myArgs[1]);
                db.save(data);
                break;
            case 'count':
                let counter = await todo_func.count(data);
                console.log(' number of tasks is: ', counter);
                    break;
            case 'delete_t':
                myArgs.shift();
                data = await todo_func.delete_task(data, myArgs);
                db.save(data);
                break;
            case 'all':
                console.table(await todo_func.all(data));
                break;
            case 'completed':
                await todo_func.completed(data);
                break;
            case 'open_tasks':
                await todo_func.open_tasks(data);
                break;
            case 'remove_complited':
                await todo_func.remove_complited(data,myArgs[1]);
                break;
            case 'help':
            default:
                console.log(await todo_func.options());
            }
}
init();

