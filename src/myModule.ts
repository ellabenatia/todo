import fs from 'fs/promises';
import {task} from './interfaces/task.js'

const uid = () => Math.random().toString(36).substring(6);

export async function create(data:task[],args: string[]): Promise<task[]> {
    for(let todo of args){
        let task: task = {
            id: uid(),
            name: todo,
            isComleted: false
        }
        data.push(task);
    }
    return data;
}

export async function count(data:task[]){
    return data.length;

}

export async function read(data:task[]): Promise<task[]> {
    return data;
}
export async function update(data:task[],id: string): Promise<task[]> {
    for(let task of data){
        if(task.id === id){
            task.isComleted = !(task.isComleted);
        }
    }
   return data;
}

export async function delete_task(data: task[],ids: string[]): Promise<task[]> {
    for(let id of ids){
        data = data.filter((task) => task.id!==id);
    }
    return data;
}
export async function open_tasks(data:task[]): Promise<task[]>{
    let open  = data.filter((task) => task.isComleted===false);
    return open;
}
export async function completed(data:task[]):  Promise<task[]>{
    let completed_task  = data.filter((task) => task.isComleted===true);
    return completed_task;
}
export async function all(data:task[]): Promise<task[]> {
    return data;
}
export async function remove_complited(data:task[],id: string):Promise<task[]>{
     data = await open_tasks(data);
     await fs.writeFile('./todo.json', JSON.stringify(data, null, 2));
     return data;
}

export async function options(): Promise<any> {
   let data= await fs.readFile('./options.txt', 'utf8');
   //let data = JSON.parse(data_json);
   return data; 
}